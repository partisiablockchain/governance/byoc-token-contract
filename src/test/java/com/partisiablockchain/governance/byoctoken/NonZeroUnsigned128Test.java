package com.partisiablockchain.governance.byoctoken;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.math.Unsigned256;
import java.math.BigInteger;
import nl.jqno.equalsverifier.EqualsVerifier;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

final class NonZeroUnsigned128Test {

  private static final BigInteger MAX_VALUE = BigInteger.TWO.pow(128).subtract(BigInteger.ONE);

  @Test
  void constructor_ZeroUnsigned182() {
    Assertions.assertThatThrownBy(() -> new NonZeroUnsigned128(Unsigned256.ZERO))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Amount has to be positive");
  }

  @Test
  void constructor_InvalidSize() {
    Assertions.assertThatThrownBy(
            () -> new NonZeroUnsigned128(Unsigned256.create(MAX_VALUE.add(BigInteger.ONE))))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Amount cannot exceed 128 bits");
  }

  @Test
  void toString_Override() {
    Unsigned256 ten = Unsigned256.TEN;
    NonZeroUnsigned128 unsigned128 = new NonZeroUnsigned128(ten);
    assertThat(unsigned128.toString()).isEqualTo(ten.toString());
  }

  @Test
  void equals_Override() {
    EqualsVerifier.forClass(NonZeroUnsigned128.class).withNonnullFields("value").verify();
  }

  @Test
  void hashCode_Override() {
    Unsigned256 ten = Unsigned256.TEN;
    NonZeroUnsigned128 unsigned128 = new NonZeroUnsigned128(ten);
    assertThat(unsigned128.hashCode()).isEqualTo(ten.hashCode());
  }
}
