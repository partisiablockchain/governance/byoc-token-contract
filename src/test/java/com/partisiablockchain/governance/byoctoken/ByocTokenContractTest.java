package com.partisiablockchain.governance.byoctoken;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.contract.CallbackContext;
import com.partisiablockchain.contract.SysContractContextTest;
import com.partisiablockchain.contract.SysContractSerialization;
import com.partisiablockchain.contract.sys.LocalPluginStateUpdate;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateAccessorAvlLeafNode;
import com.partisiablockchain.tree.AvlTree;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.immutable.FixedList;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Function;
import org.junit.jupiter.api.Test;

final class ByocTokenContractTest {

  static final BlockchainAddress ADDRESS_A =
      BlockchainAddress.fromHash(BlockchainAddress.Type.ACCOUNT, Hash.create(h -> h.writeInt(1)));
  static final BlockchainAddress ADDRESS_B =
      BlockchainAddress.fromHash(BlockchainAddress.Type.ACCOUNT, Hash.create(h -> h.writeInt(2)));
  static final BlockchainAddress ADDRESS_C =
      BlockchainAddress.fromHash(BlockchainAddress.Type.ACCOUNT, Hash.create(h -> h.writeInt(3)));
  private static final BlockchainAddress sender = ADDRESS_A;
  private static final BlockchainAddress senderContract =
      BlockchainAddress.fromHash(
          BlockchainAddress.Type.CONTRACT_PUBLIC, Hash.create(h -> h.writeInt(1)));
  private static final BlockchainAddress receiver = ADDRESS_B;
  private static final BlockchainAddress spender = ADDRESS_C;
  private static final Hash HASH_A = Hash.create(h -> h.writeInt(123));
  private static final String BYOC_SYMBOL = "ETH";

  private final SysContractSerialization<ByocTokenContractState> serialization =
      new SysContractSerialization<>(ByocTokenContractInvoker.class, ByocTokenContractState.class);
  private SysContractContextTest context = new SysContractContextTest();

  @Test
  void onCreate() {
    ByocTokenContractState initial = serialization.create(context, s -> s.writeString(BYOC_SYMBOL));
    assertThat(initial).isNotNull();
    assertThat(initial.getByocSymbol()).isEqualTo(BYOC_SYMBOL);
  }

  @Test
  void onUpgrade() {
    final String symbol = "UPDATED_SYMBOL";
    final Hash transactionHash = HASH_A;
    final NonZeroUnsigned128 amount = new NonZeroUnsigned128(Unsigned256.ONE);

    PendingTransfer pendingTransfer = new PendingTransfer(ADDRESS_A, ADDRESS_B, ADDRESS_C, amount);
    AvlTree<Hash, PendingTransfer> pendingTransfers =
        AvlTree.create(Map.of(transactionHash, pendingTransfer));

    BlockchainAddress from = ADDRESS_A;
    BlockchainAddress spender = ADDRESS_B;
    Allowances allowance = Allowances.initial().approve(spender, amount);
    AvlTree<BlockchainAddress, Allowances> allowances = AvlTree.create(Map.of(from, allowance));

    ByocTokenContractState beforeUpgrade =
        new ByocTokenContractState(symbol, allowances, pendingTransfers);
    ByocTokenContractState afterUpgrade =
        new ByocTokenContract().upgrade(StateAccessor.create(beforeUpgrade));

    assertThat(afterUpgrade.getByocSymbol()).isEqualTo(symbol);
    assertThat(afterUpgrade.getPendingTransfer(transactionHash)).isEqualTo(pendingTransfer);
    assertThat(afterUpgrade.getAllowances(from).getAllowance(spender)).isEqualTo(amount.getValue());
  }

  @Test
  void getShortName() {
    assertThat(ByocTokenContract.Invocations.TRANSFER).isEqualTo((byte) 0x01);
    assertThat(ByocTokenContract.Callbacks.TRANSFER_CALLBACK).isEqualTo((byte) 0x01);
  }

  @Test
  void onInvoke_Approve() {
    final BlockchainAddress spender = ADDRESS_B;
    final Unsigned256 amount = Unsigned256.TEN;
    final BlockchainAddress from = ADDRESS_A;

    from(from);
    ByocTokenContractState afterInitiateApprove = invokeApprove(spender, amount, initial());

    assertThat(afterInitiateApprove.getAllowances(from).getAllowance(spender)).isEqualTo(amount);
  }

  @Test
  void onInvoke_Approve_CalledAgainShouldOverwrite() {
    final BlockchainAddress spender = ADDRESS_B;
    final Unsigned256 amount = Unsigned256.TEN;
    final Unsigned256 overwrittenAmount = Unsigned256.ONE;
    final BlockchainAddress from = ADDRESS_A;

    from(from);
    ByocTokenContractState afterInitiateApprove = invokeApprove(spender, amount, initial());
    assertThat(afterInitiateApprove.getAllowances(from).getAllowance(spender)).isEqualTo(amount);

    ByocTokenContractState overwrittenApprove =
        invokeApprove(spender, overwrittenAmount, afterInitiateApprove);
    assertThat(overwrittenApprove.getAllowances(from).getAllowance(spender))
        .isEqualTo(overwrittenAmount);
  }

  @Test
  void onInvoke_Approve_Invalid_NonPositiveAmount() {
    final Unsigned256 zeroAmount = Unsigned256.ZERO;

    from(ADDRESS_A);
    assertThatThrownBy(() -> invokeApprove(ADDRESS_B, zeroAmount, initial()))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Amount has to be positive");
  }

  @Test
  void onInvoke_Transfer() {
    final NonZeroUnsigned128 amount = new NonZeroUnsigned128(Unsigned256.ONE);
    final Hash transactionHash = context.getCurrentTransactionHash();

    from(sender);
    invokeTransfer(receiver, amount.getValue(), initial());

    assertRpc(
        context.getRemoteCalls().callbackRpc,
        ByocTokenContract.Callbacks.transferCallback(transactionHash));

    assertWithdrawDeposit(sender, receiver, transactionHash, amount);
  }

  @Test
  void onInvoke_TransferFromContract() {
    final NonZeroUnsigned128 amount = new NonZeroUnsigned128(Unsigned256.ONE);
    final Hash transactionHash = context.getCurrentTransactionHash();

    from(senderContract);
    invokeTransfer(receiver, amount.getValue(), initial());

    assertRpc(
        context.getRemoteCalls().callbackRpc,
        ByocTokenContract.Callbacks.transferCallback(transactionHash));

    assertWithdrawDeposit(senderContract, receiver, transactionHash, amount);
  }

  @Test
  void onInvoke_TransferToSame() {
    final NonZeroUnsigned128 amount = new NonZeroUnsigned128(Unsigned256.ONE);
    from(sender);
    assertThatThrownBy(() -> invokeTransfer(sender, amount.getValue(), initial()))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Cannot transfer between same account");
  }

  @Test
  void onInvoke_Transfer_Commit() {
    final Unsigned256 amount = Unsigned256.ONE;
    final Hash transactionHash = context.getCurrentTransactionHash();

    from(sender);
    ByocTokenContractState afterInitiateTransfer = invokeTransfer(receiver, amount, initial());

    ByocTokenContractState transferSucceeded =
        transferCallback(List.of(true, true), transactionHash, afterInitiateTransfer);

    assertThat(transferSucceeded.getPendingTransfer(transactionHash)).isNull();

    assertCommit(sender, receiver, transactionHash);
  }

  @Test
  void onInvoke_Transfer_Abort() {
    final Unsigned256 amount = Unsigned256.ONE;
    final Hash transactionHash = context.getCurrentTransactionHash();

    from(sender);
    ByocTokenContractState afterInitiateTransfer = invokeTransfer(receiver, amount, initial());

    ByocTokenContractState transferFailed =
        transferCallback(List.of(false, true), transactionHash, afterInitiateTransfer);

    assertThat(transferFailed.getPendingTransfer(transactionHash)).isNull();

    assertThat(transferFailed.getAllowances(sender).getAllowance(sender))
        .isEqualTo(Unsigned256.ZERO);

    assertAbort(sender, receiver, transactionHash);
  }

  @Test
  void onInvoke_Transfer_Invalid_NonPositiveAmount() {
    final Unsigned256 zeroAmount = Unsigned256.ZERO;

    from(ADDRESS_A);
    assertThatThrownBy(() -> invokeTransfer(ADDRESS_C, zeroAmount, initial()))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Amount has to be positive");
  }

  @Test
  void onInvoke_TransferFrom() {
    final Unsigned256 approveAmount = Unsigned256.TEN;
    final NonZeroUnsigned128 transferAmount = new NonZeroUnsigned128(Unsigned256.ONE);
    final Hash transactionHash = context.getCurrentTransactionHash();

    from(sender);
    ByocTokenContractState afterApprove = invokeApprove(spender, approveAmount, initial());
    assertThat(afterApprove.getAllowances(sender).getAllowance(spender)).isEqualTo(approveAmount);

    from(spender);
    ByocTokenContractState afterInitiateTransfer =
        invokeTransferFrom(sender, receiver, transferAmount.getValue(), afterApprove);
    assertThat(afterInitiateTransfer.getAllowances(sender).getAllowance(spender))
        .isEqualTo(approveAmount.subtract(transferAmount.getValue()));

    PendingTransfer pendingTransfer = afterInitiateTransfer.getPendingTransfer(transactionHash);
    assertThat(pendingTransfer.spender()).isEqualTo(spender);
    assertThat(pendingTransfer.sender()).isEqualTo(sender);
    assertThat(pendingTransfer.receiver()).isEqualTo(receiver);
    assertThat(pendingTransfer.amount()).isEqualTo(transferAmount);

    assertRpc(
        context.getRemoteCalls().callbackRpc,
        ByocTokenContract.Callbacks.transferCallback(transactionHash));

    assertWithdrawDeposit(sender, receiver, transactionHash, transferAmount);
  }

  @Test
  void onInvoke_TransferFrom_CleanUpAllowance() {
    final Unsigned256 amount = Unsigned256.TEN;

    from(sender);
    ByocTokenContractState afterApprove = invokeApprove(spender, amount, initial());
    assertThat(afterApprove.getAllowances(sender).getAllowance(spender)).isEqualTo(amount);

    from(spender);
    ByocTokenContractState afterInitiateTransferFrom =
        invokeTransferFrom(sender, receiver, amount, afterApprove);

    // Ensure state is cleaned up, since the full approved amount was transferred
    int numberOfAllowances =
        StateAccessor.create(afterInitiateTransferFrom).get("allowed").getTreeLeaves().size();
    assertThat(numberOfAllowances).isEqualTo(0);
  }

  @Test
  void onInvoke_TransferFrom_AllowanceDoesNotOverflow() {
    final Unsigned256 maxAllowance = NonZeroUnsigned128.MAX_VALID_VALUE;
    from(sender);
    ByocTokenContractState state = invokeApprove(spender, maxAllowance, initial());
    from(spender);
    Unsigned256 transferAmount = Unsigned256.create(1);
    state = invokeTransferFrom(sender, receiver, transferAmount, state);
    from(sender);
    // Re-approve maximum value
    state = invokeApprove(spender, maxAllowance, state);
    // "Resume" callback, which re-adds allowance from previous transfer, and ensure no overflow
    // happens
    state = transferCallback(List.of(true, false), context.getCurrentTransactionHash(), state);
    assertThat(state.getAllowances(sender).getAllowance(spender)).isEqualTo(maxAllowance);
  }

  @Test
  void onInvoke_TransferFrom_CleanUpAllowance_MultipleAllowances() {
    final Unsigned256 amount = Unsigned256.TEN;
    final Unsigned256 otherAmount = Unsigned256.TEN;
    final BlockchainAddress otherSpender =
        BlockchainAddress.fromHash(
            BlockchainAddress.Type.ACCOUNT, Hash.create(h -> h.writeString("Other")));

    from(sender);
    ByocTokenContractState afterFirstApprove = invokeApprove(spender, amount, initial());
    ByocTokenContractState afterSecondApprove =
        invokeApprove(otherSpender, otherAmount, afterFirstApprove);
    assertThat(afterSecondApprove.getAllowances(sender).getAllowance(spender)).isEqualTo(amount);
    assertThat(afterSecondApprove.getAllowances(sender).getAllowance(otherSpender))
        .isEqualTo(amount);

    from(spender);
    ByocTokenContractState afterInitiateTransferFrom =
        invokeTransferFrom(sender, receiver, amount, afterSecondApprove);

    // Ensure other allowances are kept after clean up
    List<StateAccessorAvlLeafNode> allowances =
        StateAccessor.create(afterInitiateTransferFrom).get("allowed").getTreeLeaves();

    int numberOfAllowances = allowances.size();
    assertThat(numberOfAllowances).isEqualTo(1);

    StateAccessorAvlLeafNode allowance = allowances.get(0);
    BlockchainAddress addressFrom = allowance.getKey().blockchainAddressValue();
    assertThat(addressFrom).isEqualTo(sender);
    Unsigned256 amountOther =
        allowance.getValue().cast(Allowances.class).getAllowance(otherSpender);
    assertThat(amountOther).isEqualTo(otherAmount);
  }

  @Test
  void onInvoke_TransferFrom_SameSpenderAndFrom() {
    final NonZeroUnsigned128 amount = new NonZeroUnsigned128(Unsigned256.ONE);
    final Hash transactionHash = context.getCurrentTransactionHash();

    from(sender);
    ByocTokenContractState afterApprove = invokeApprove(sender, amount.getValue(), initial());
    assertThat(afterApprove.getAllowances(sender).getAllowance(sender))
        .isEqualTo(amount.getValue());

    ByocTokenContractState afterInitiateTransfer =
        invokeTransferFrom(sender, receiver, amount.getValue(), afterApprove);
    assertThat(afterInitiateTransfer.getAllowances(sender).getAllowance(sender))
        .isEqualTo(Unsigned256.ZERO);

    PendingTransfer pendingTransfer = afterInitiateTransfer.getPendingTransfer(transactionHash);
    assertThat(pendingTransfer.spender()).isEqualTo(sender);
    assertThat(pendingTransfer.sender()).isEqualTo(sender);
    assertThat(pendingTransfer.receiver()).isEqualTo(receiver);
    assertThat(pendingTransfer.amount()).isEqualTo(amount);

    assertWithdrawDeposit(sender, receiver, transactionHash, amount);
  }

  @Test
  void onInvoke_TransferFrom_Invalid_NotAllowed() {
    from(ADDRESS_A);
    assertThatThrownBy(() -> invokeTransferFrom(ADDRESS_B, ADDRESS_C, Unsigned256.TEN, initial()))
        .isInstanceOf(RuntimeException.class)
        .hasMessage(
            "The spender was not allowed to transfer the specified amount on behalf of the"
                + " account");
  }

  @Test
  void onInvoke_TransferFrom_Commit() {
    final Unsigned256 amount = Unsigned256.TEN;
    final Hash transactionHash = context.getCurrentTransactionHash();

    from(sender);
    ByocTokenContractState afterApprove = invokeApprove(spender, amount, initial());
    assertThat(afterApprove.getAllowances(sender).getAllowance(spender)).isEqualTo(amount);

    from(spender);
    ByocTokenContractState afterInitiateTransfer =
        invokeTransferFrom(sender, receiver, amount, afterApprove);

    ByocTokenContractState transferSucceeded =
        transferCallback(List.of(true, true), transactionHash, afterInitiateTransfer);

    assertThat(transferSucceeded.getPendingTransfer(transactionHash)).isNull();

    assertThat(transferSucceeded.getAllowances(sender).getAllowance(spender))
        .isEqualTo(Unsigned256.ZERO);

    assertCommit(sender, receiver, transactionHash);
  }

  @Test
  void onInvoke_TransferFrom_Abort() {
    final Unsigned256 amount = Unsigned256.TEN;
    final Hash transactionHash = context.getCurrentTransactionHash();

    from(sender);
    ByocTokenContractState afterApproveCallback = invokeApprove(spender, amount, initial());
    assertThat(afterApproveCallback.getAllowances(sender).getAllowance(spender)).isEqualTo(amount);

    from(spender);
    ByocTokenContractState afterInitiateTransfer =
        invokeTransferFrom(sender, receiver, amount, afterApproveCallback);

    ByocTokenContractState transferFailed =
        transferCallback(List.of(true, false), transactionHash, afterInitiateTransfer);

    assertThat(transferFailed.getPendingTransfer(transactionHash)).isNull();

    assertThat(transferFailed.getAllowances(sender).getAllowance(spender)).isEqualTo(amount);

    assertAbort(sender, receiver, transactionHash);
  }

  @Test
  void onInvoke_Abort_Transfer() {
    final NonZeroUnsigned128 amount = new NonZeroUnsigned128(Unsigned256.ONE);
    final Hash transactionHash = context.getCurrentTransactionHash();

    from(sender);
    ByocTokenContractState afterInitiateTransfer =
        invokeTransfer(receiver, amount.getValue(), initial());

    ByocTokenContractState afterInvokeAbort = invokeAbort(transactionHash, afterInitiateTransfer);

    assertThat(afterInvokeAbort.getPendingTransfer(transactionHash)).isNull();

    assertAbort(sender, receiver, transactionHash);
  }

  @Test
  void onInvoke_Abort_TransferFrom() {
    final Unsigned256 amount = Unsigned256.TEN;
    final Hash transactionHash = context.getCurrentTransactionHash();

    from(sender);
    ByocTokenContractState afterApprove = invokeApprove(spender, amount, initial());
    assertThat(afterApprove.getAllowances(sender).getAllowance(spender)).isEqualTo(amount);

    from(spender);
    ByocTokenContractState afterInitiateTransferFrom =
        invokeTransferFrom(sender, receiver, amount, afterApprove);

    from(sender);
    ByocTokenContractState afterInvokeAbort =
        invokeAbort(transactionHash, afterInitiateTransferFrom);

    assertThat(afterInvokeAbort.getPendingTransfer(transactionHash)).isNull();
    assertThat(afterInvokeAbort.getAllowances(sender).getAllowance(spender)).isEqualTo(amount);

    from(spender);
    ByocTokenContractState afterInvokeAbortSpender =
        invokeAbort(transactionHash, afterInitiateTransferFrom);

    assertThat(afterInvokeAbortSpender.getPendingTransfer(transactionHash)).isNull();
    assertThat(afterInvokeAbortSpender.getAllowances(sender).getAllowance(spender))
        .isEqualTo(amount);
  }

  @Test
  void onInvoke_Abort_TransferDoesNotExist() {
    assertThatThrownBy(() -> invokeAbort(HASH_A, initial()))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Transfer does not exist");
  }

  @Test
  void onInvoke_Abort_NotSender() {
    final Hash transactionHash = context.getCurrentTransactionHash();

    from(sender);
    ByocTokenContractState afterInitiateTransfer =
        invokeTransfer(receiver, Unsigned256.TEN, initial());

    from(receiver);
    assertThatThrownBy(() -> invokeAbort(transactionHash, afterInitiateTransfer))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("A pending transfer may only be aborted by the sender or the spender");
  }

  @Test
  void invoke_transfer_checkRpc() {
    byte[] serializedValue = fromUnsigned256(Unsigned256.TEN);

    byte[] rpc =
        SafeDataOutputStream.serialize(
            s -> {
              s.writeByte(ByocTokenContract.Invocations.TRANSFER);
              sender.write(s);
              s.write(serializedValue);
            });

    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    assertThat(stream.readUnsignedByte()).isEqualTo(ByocTokenContract.Invocations.TRANSFER);
    assertThat(BlockchainAddress.read(stream)).isEqualTo(sender);
    Unsigned256 value = Unsigned256.create(stream.readBytes(16));
    assertThat(value).isEqualTo(Unsigned256.TEN);
  }

  private void from(BlockchainAddress from) {
    context =
        new SysContractContextTest(context.getBlockProductionTime(), context.getBlockTime(), from);
  }

  private static ByocTokenContractState initial() {
    return ByocTokenContractState.initial(BYOC_SYMBOL);
  }

  private void assertRpc(byte[] actual, Consumer<SafeDataOutputStream> expected) {
    assertThat(actual).isEqualTo(SafeDataOutputStream.serialize(expected));
  }

  private ByocTokenContractState invokeApprove(
      BlockchainAddress spender, Unsigned256 amount, ByocTokenContractState state) {
    byte[] amountBytes = fromUnsigned256(amount);
    return serialization.invoke(
        context,
        state,
        rpc -> {
          rpc.writeByte(ByocTokenContract.Invocations.APPROVE);
          spender.write(rpc);
          rpc.write(amountBytes);
        });
  }

  private ByocTokenContractState invokeTransfer(
      BlockchainAddress to, Unsigned256 amount, ByocTokenContractState state) {
    byte[] amountBytes = fromUnsigned256(amount);
    return serialization.invoke(
        context,
        state,
        rpc -> {
          rpc.writeByte(ByocTokenContract.Invocations.TRANSFER);
          to.write(rpc);
          rpc.write(amountBytes);
        });
  }

  private ByocTokenContractState invokeTransferFrom(
      BlockchainAddress sender,
      BlockchainAddress receiver,
      Unsigned256 amount,
      ByocTokenContractState state) {
    byte[] amountBytes = fromUnsigned256(amount);
    return serialization.invoke(
        context,
        state,
        rpc -> {
          rpc.writeByte(ByocTokenContract.Invocations.TRANSFER_FROM);
          sender.write(rpc);
          receiver.write(rpc);
          rpc.write(amountBytes);
        });
  }

  static byte[] fromUnsigned256(Unsigned256 unsigned256) {
    byte[] unsigned128 = new byte[16];
    System.arraycopy(unsigned256.serialize(), 16, unsigned128, 0, 16);
    return unsigned128;
  }

  private ByocTokenContractState invokeAbort(Hash transactionHash, ByocTokenContractState state) {
    return serialization.invoke(
        context,
        state,
        rpc -> {
          rpc.writeByte(ByocTokenContract.Invocations.ABORT);
          transactionHash.write(rpc);
        });
  }

  private ByocTokenContractState transferCallback(
      List<Boolean> results, Hash transactionHash, ByocTokenContractState state) {
    return serialization.callback(
        context,
        createCallbackContext(results),
        state,
        ByocTokenContract.Callbacks.transferCallback(transactionHash));
  }

  private CallbackContext createCallbackContext(List<Boolean> results) {
    Function<Boolean, CallbackContext.ExecutionResult> createExecutionResult =
        success ->
            CallbackContext.createResult(
                HASH_A, success, SafeDataInputStream.createFromBytes(new byte[0]));
    return CallbackContext.create(FixedList.create(results.stream().map(createExecutionResult)));
  }

  private void assertWithdrawDeposit(
      BlockchainAddress sender,
      BlockchainAddress receiver,
      Hash transactionHash,
      NonZeroUnsigned128 amount) {
    List<LocalPluginStateUpdate> accountPluginInvocations =
        context.getUpdateLocalAccountPluginStates();
    assertThat(accountPluginInvocations).hasSize(2);

    LocalPluginStateUpdate withdraw = accountPluginInvocations.get(0);
    LocalPluginStateUpdate deposit = accountPluginInvocations.get(1);

    assertThat(withdraw.getContext()).isEqualTo(sender);
    assertThat(deposit.getContext()).isEqualTo(receiver);
    assertThat(withdraw.getRpc())
        .isEqualTo(
            AccountPluginRpc.initiateWithdraw(
                sender.getType() != BlockchainAddress.Type.ACCOUNT,
                transactionHash,
                amount,
                BYOC_SYMBOL));
    assertThat(deposit.getRpc())
        .isEqualTo(AccountPluginRpc.initiateDeposit(transactionHash, amount, BYOC_SYMBOL));
  }

  private void assertCommit(
      BlockchainAddress sender, BlockchainAddress receiver, Hash transactionHash) {
    List<LocalPluginStateUpdate> accountPluginInvocations =
        context.getUpdateLocalAccountPluginStates();
    assertThat(accountPluginInvocations).hasSize(4);

    LocalPluginStateUpdate commitFrom = accountPluginInvocations.get(2);
    LocalPluginStateUpdate commitTo = accountPluginInvocations.get(3);

    byte[] commitTransferRpc = AccountPluginRpc.commitTransfer(transactionHash);
    assertAccountPluginInvocations(commitFrom, commitTo, sender, receiver, commitTransferRpc);
  }

  private void assertAbort(
      BlockchainAddress sender, BlockchainAddress receiver, Hash transactionHash) {
    List<LocalPluginStateUpdate> accountPluginInvocations =
        context.getUpdateLocalAccountPluginStates();
    assertThat(accountPluginInvocations).hasSize(4);

    LocalPluginStateUpdate abortFrom = accountPluginInvocations.get(2);
    LocalPluginStateUpdate abortTo = accountPluginInvocations.get(3);

    byte[] abortTransferRpc = AccountPluginRpc.abortTransfer(transactionHash);
    assertAccountPluginInvocations(abortFrom, abortTo, sender, receiver, abortTransferRpc);
  }

  private void assertAccountPluginInvocations(
      LocalPluginStateUpdate sender,
      LocalPluginStateUpdate receiver,
      BlockchainAddress fromContext,
      BlockchainAddress toContext,
      byte[] rpc) {
    assertThat(sender.getContext()).isEqualTo(fromContext);
    assertThat(receiver.getContext()).isEqualTo(toContext);
    assertThat(sender.getRpc()).isEqualTo(rpc);
    assertThat(receiver.getRpc()).isEqualTo(rpc);
  }
}
