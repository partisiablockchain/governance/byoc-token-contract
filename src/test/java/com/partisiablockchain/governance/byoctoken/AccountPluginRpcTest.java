package com.partisiablockchain.governance.byoctoken;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.math.Unsigned256;
import com.secata.stream.SafeDataInputStream;
import org.junit.jupiter.api.Test;

final class AccountPluginRpcTest {

  @Test
  void initiateWithdrawForContract() {
    Hash transactionHash = Hash.create(h -> h.writeInt(123));
    NonZeroUnsigned128 amount = new NonZeroUnsigned128(Unsigned256.TEN);
    String symbol = "SYMBOL";

    byte[] rpc = AccountPluginRpc.initiateWithdraw(true, transactionHash, amount, symbol);

    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    assertThat(stream.readUnsignedByte())
        .isEqualTo(AccountPluginRpc.INITIATE_BYOC_TRANSFER_SENDER_OLD);
    assertThat(Hash.read(stream)).isEqualTo(transactionHash);
    assertThat(Unsigned256.read(stream)).isEqualTo(amount.getValue());
    assertThat(stream.readString()).isEqualTo(symbol);
  }

  @Test
  void initiateWithdrawForAccount() {
    Hash transactionHash = Hash.create(h -> h.writeInt(123));
    NonZeroUnsigned128 amount = new NonZeroUnsigned128(Unsigned256.TEN);
    String symbol = "SYMBOL";

    byte[] rpc = AccountPluginRpc.initiateWithdraw(false, transactionHash, amount, symbol);

    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    assertThat(stream.readUnsignedByte()).isEqualTo(AccountPluginRpc.INITIATE_BYOC_TRANSFER_SENDER);
    assertThat(stream.readOptional(BlockchainAddress::read)).isNull();
    assertThat(Hash.read(stream)).isEqualTo(transactionHash);
    assertThat(Unsigned256.read(stream)).isEqualTo(amount.getValue());
    assertThat(stream.readString()).isEqualTo(symbol);
  }

  @Test
  void initiateDeposit() {
    Hash transactionHash = Hash.create(h -> h.writeInt(123));
    NonZeroUnsigned128 amount = new NonZeroUnsigned128(Unsigned256.TEN);
    String symbol = "SYMBOL";

    byte[] rpc = AccountPluginRpc.initiateDeposit(transactionHash, amount, symbol);

    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    assertThat(stream.readUnsignedByte())
        .isEqualTo(AccountPluginRpc.INITIATE_BYOC_TRANSFER_RECIPIENT);
    assertThat(Hash.read(stream)).isEqualTo(transactionHash);
    assertThat(Unsigned256.read(stream)).isEqualTo(amount.getValue());
    assertThat(stream.readString()).isEqualTo(symbol);
  }

  @Test
  void commitTransfer() {
    Hash transactionHash = Hash.create(h -> h.writeInt(123));

    byte[] rpc = AccountPluginRpc.commitTransfer(transactionHash);

    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    assertThat(stream.readUnsignedByte()).isEqualTo(AccountPluginRpc.COMMIT_TRANSFER);
    assertThat(Hash.read(stream)).isEqualTo(transactionHash);
  }

  @Test
  void abortTransfer() {
    Hash transactionHash = Hash.create(h -> h.writeInt(123));

    byte[] rpc = AccountPluginRpc.abortTransfer(transactionHash);

    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    assertThat(stream.readUnsignedByte()).isEqualTo(AccountPluginRpc.ABORT_TRANSFER);
    assertThat(Hash.read(stream)).isEqualTo(transactionHash);
  }
}
