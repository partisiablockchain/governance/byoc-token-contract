package com.partisiablockchain.governance.byoctoken;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;
import java.math.BigInteger;

/** Custom implementation of unsigned non-zero 128 bit integer. */
@Immutable
final class NonZeroUnsigned128 implements StateSerializable {

  static final Unsigned256 MAX_VALID_VALUE =
      Unsigned256.create(BigInteger.ONE.shiftLeft(128).subtract(BigInteger.ONE));

  private final Unsigned256 value;

  @SuppressWarnings("unused")
  private NonZeroUnsigned128() {
    value = null;
  }

  NonZeroUnsigned128(Unsigned256 unsigned128) {
    ensureValidSize(unsigned128);
    ensurePositive(unsigned128);
    this.value = unsigned128;
  }

  private void ensureValidSize(Unsigned256 amount) {
    if (amount.compareTo(MAX_VALID_VALUE) > 0) {
      throw new IllegalArgumentException("Amount cannot exceed 128 bits");
    }
  }

  private void ensurePositive(Unsigned256 amount) {
    if (amount.compareTo(Unsigned256.ZERO) == 0) {
      throw new IllegalArgumentException("Amount has to be positive");
    }
  }

  Unsigned256 getValue() {
    return value;
  }

  @Override
  public String toString() {
    return value.toString();
  }

  @Override
  public boolean equals(Object other) {
    if (other == null || getClass() != other.getClass()) {
      return false;
    }
    return value.equals(((NonZeroUnsigned128) other).value);
  }

  @Override
  public int hashCode() {
    return value.hashCode();
  }

  static NonZeroUnsigned128 createFromStateAccessor(StateAccessor oldState) {
    return new NonZeroUnsigned128(oldState.get("value").cast(Unsigned256.class));
  }
}
