package com.partisiablockchain.governance.byoctoken;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.contract.CallbackContext;
import com.partisiablockchain.contract.reflect.Action;
import com.partisiablockchain.contract.reflect.AutoSysContract;
import com.partisiablockchain.contract.reflect.Callback;
import com.partisiablockchain.contract.reflect.Init;
import com.partisiablockchain.contract.reflect.RpcType;
import com.partisiablockchain.contract.reflect.Upgrade;
import com.partisiablockchain.contract.sys.LocalPluginStateUpdate;
import com.partisiablockchain.contract.sys.SysContractContext;
import com.partisiablockchain.contract.sys.SystemEventCreator;
import com.partisiablockchain.contract.sys.SystemEventManager;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.StateAccessor;
import com.secata.stream.DataStreamSerializable;
import com.secata.tools.immutable.FixedList;

/** BYOC token contract. */
@AutoSysContract(ByocTokenContractState.class)
public final class ByocTokenContract {

  static final long TWO_PHASE_CALLBACK_GAS_COST =
      2500 /* callback CPU cost */ + 2 * 2500 /* COMMIT/ABORT event cost */;

  /**
   * Initialize the byoc token contract.
   *
   * @param symbol coin symbol that is transferred
   * @return initial state
   */
  @Init
  public ByocTokenContractState create(String symbol) {
    return ByocTokenContractState.initial(symbol);
  }

  /**
   * Migrate an existing contract to this version.
   *
   * @param oldState accessor for the old state
   * @return the migrated state
   */
  @Upgrade
  public ByocTokenContractState upgrade(StateAccessor oldState) {
    return ByocTokenContractState.migrateState(oldState);
  }

  /** Invocation shortnames. */
  static final class Invocations {
    static final int TRANSFER = 0x01;
    static final int TRANSFER_FROM = 0x03;
    static final int APPROVE = 0x05;
    static final int ABORT = 0x06;

    private Invocations() {}
  }

  /** Callback shortnames. */
  static final class Callbacks {
    static final int TRANSFER_CALLBACK = 0x01;

    private Callbacks() {}

    static DataStreamSerializable transferCallback(Hash transactionId) {
      return stream -> {
        stream.writeByte(ByocTokenContract.Callbacks.TRANSFER_CALLBACK);
        transactionId.write(stream);
      };
    }
  }

  /**
   * Transfers <var>x</var> BYOC to address <var>receiver</var> from the caller's address
   * <var>sender</var>. The transfer is executed as a two-phase commit. The invocation requires the
   * remaining gas of the transaction to be greater than or equal to {@link
   * #TWO_PHASE_CALLBACK_GAS_COST}, or else the transaction is rejected. <var>receiver</var> and
   * <var>x</var> is read from stream.
   *
   * @pre
   *     <ul>
   *       <li><var>sender</var> and <var>receiver</var> are different accounts.
   *       <li>The amount <var>x &gt; 0</var>.
   *       <li><var>sender</var> has <var>y &ge; x</var> BYOC.
   *       <li><var>receiver</var> has <var>z</var> BYOC.
   *     </ul>
   *
   * @post
   *     <ul>
   *       <li><var>sender</var> balance is <var>y - x</var> BYOC.
   *       <li><var>receiver</var> balance is <var>z + x</var> BYOC.
   *     </ul>
   *
   * @param context execution context
   * @param state current state
   * @param receiver receiver of tokens
   * @param amount number of tokens to transfer
   * @return updated state
   */
  @Action(Invocations.TRANSFER)
  public ByocTokenContractState transfer(
      SysContractContext context,
      ByocTokenContractState state,
      BlockchainAddress receiver,
      @RpcType(size = 16, signed = false) Unsigned256 amount) {
    final BlockchainAddress sender = context.getFrom();
    final Hash transactionHash = context.getCurrentTransactionHash();

    SystemEventManager remoteCallsCreator = context.getRemoteCallsCreator();
    initiateTransfer(
        sender,
        receiver,
        new NonZeroUnsigned128(amount),
        transactionHash,
        state.getByocSymbol(),
        remoteCallsCreator);
    return state.addPendingTransfer(
        transactionHash, sender, sender, receiver, new NonZeroUnsigned128(amount));
  }

  /**
   * Transfers <var>x</var> BYOC to address <var>receiver</var> from address <var>sender</var>. This
   * invocation requires that the caller is allowed to spend at least <var>x</var> on behalf of
   * <var>sender</var>, see {@link #approve}. The transfer is executed as a two-phase commit. The
   * invocation requires the remaining gas of the transaction to be greater than or equal to {@link
   * #TWO_PHASE_CALLBACK_GAS_COST}, or else the transaction is rejected. <var>sender</var>,
   * <var>receiver</var> and <var>x</var> is read from stream.
   *
   * @pre
   *     <ul>
   *       <li><var>sender</var> and <var>receiver</var> are different accounts.
   *       <li>The amount <var>x &gt; 0</var>.
   *       <li><var>sender</var> has <var>y &ge; x</var> BYOC.
   *       <li><var>receiver</var> has <var>z</var> BYOC.
   *       <li>The caller has allowance to spend <var>k &ge; x</var> BYOC on behalf of
   *           <var>sender</var>.
   *     </ul>
   *
   * @post
   *     <ul>
   *       <li><var>sender</var> balance is <var>y - x</var> BYOC.
   *       <li><var>receiver</var> balance is <var>z + x</var> BYOC.
   *       <li>The caller has allowance to spend <var>k - x</var> BYOC on behalf of
   *           <var>sender</var>.
   *     </ul>
   *
   * @param context execution context
   * @param state current state
   * @param sender sender of tokens
   * @param receiver receiver of tokens
   * @param amount number of tokens to transfer
   * @return updated state
   */
  @Action(Invocations.TRANSFER_FROM)
  public ByocTokenContractState transferFrom(
      SysContractContext context,
      ByocTokenContractState state,
      BlockchainAddress sender,
      BlockchainAddress receiver,
      @RpcType(size = 16, signed = false) Unsigned256 amount) {
    final BlockchainAddress spender = context.getFrom();
    final Hash transactionHash = context.getCurrentTransactionHash();

    Unsigned256 allowance = state.getAllowances(sender).getAllowance(spender);
    ensure(
        allowance.compareTo(amount) >= 0,
        "The spender was not allowed to transfer the specified amount on behalf of the"
            + " account");
    ByocTokenContractState updatedState =
        state.reduce(spender, sender, new NonZeroUnsigned128(amount));

    SystemEventManager remoteCallsCreator = context.getRemoteCallsCreator();
    initiateTransfer(
        sender,
        receiver,
        new NonZeroUnsigned128(amount),
        transactionHash,
        state.getByocSymbol(),
        remoteCallsCreator);
    return updatedState.addPendingTransfer(
        transactionHash, spender, sender, receiver, new NonZeroUnsigned128(amount));
  }

  /**
   * Approves <var>spender</var> to transfer <var>x</var> BYOC on behalf of the caller.
   * <var>spender</var> and <var>x</var> is read from stream.
   *
   * @pre
   *     <ul>
   *       <li>The caller has BYOC &ge; x
   *       <li>The amount <var>x &gt; 0</var>
   *     </ul>
   *
   * @post
   *     <ul>
   *       <li><var>spender</var> is allowed to spend <var>x</var> on behalf of the caller
   *     </ul>
   *
   * @param context execution context
   * @param state current state
   * @param spender account getting allowance to spend money on behalf of sender
   * @param amount number of tokens allowed to transfer on behalf of sender
   * @return updated state
   */
  @Action(Invocations.APPROVE)
  public ByocTokenContractState approve(
      SysContractContext context,
      ByocTokenContractState state,
      BlockchainAddress spender,
      @RpcType(size = 16, signed = false) Unsigned256 amount) {
    final BlockchainAddress sender = context.getFrom();

    return state.approve(sender, spender, new NonZeroUnsigned128(amount));
  }

  /**
   * Aborts the pending transfer with transaction identifier <var>transactionHash</var>. Can be
   * called by <var>sender</var> or <var>spender</var> of the pending transaction, if the two-phase
   * commit failed. This operation reverts the transaction such that the amount <var>x</var> is
   * returned to <var>sender</var>. The transaction identifier <var>transactionHash</var> is read
   * from stream.
   *
   * @pre
   *     <ul>
   *       <li>An existing pending transfer matching the provided identifier
   *           <var>transactionHash</var> exists in state.
   *       <li><var>sender</var> has <var>y</var> BYOC.
   *       <li><var>receiver</var> has <var>z</var> BYOC.
   *     </ul>
   *
   * @post
   *     <ul>
   *       <li><var>sender</var> has <var>y + x</var> BYOC.
   *       <li><var>receiver</var> still has <var>z</var> BYOC.
   *       <li>Transfer event is removed from state.
   *     </ul>
   *
   * @param context execution context
   * @param state current state
   * @param transactionHash the id of the original transaction that is to be aborted
   * @return updated state
   */
  @Action(Invocations.ABORT)
  public ByocTokenContractState abort(
      SysContractContext context, ByocTokenContractState state, Hash transactionHash) {

    PendingTransfer pendingTransfer = state.getPendingTransfer(transactionHash);
    ensure(pendingTransfer != null, "Transfer does not exist");
    ensure(
        pendingTransfer.sender().equals(context.getFrom())
            || pendingTransfer.spender().equals(context.getFrom()),
        "A pending transfer may only be aborted by the sender or the spender");

    BlockchainAddress receiver = pendingTransfer.receiver();
    BlockchainAddress sender = pendingTransfer.sender();
    BlockchainAddress spender = pendingTransfer.spender();
    NonZeroUnsigned128 amount = pendingTransfer.amount();

    ByocTokenContractState updatedState = state.removePendingTransfer(transactionHash);
    SystemEventCreator invocationCreator = context.getInvocationCreator();
    if (!sender.equals(spender)) {
      // Re-add allowance if a TRANSFER_FROM was aborted
      updatedState = reAddAllowance(updatedState, sender, spender, amount);
    }
    abortTransfer(transactionHash, sender, receiver, invocationCreator);
    return updatedState;
  }

  /**
   * Callback for transfer of tokens.
   *
   * @param context execution context
   * @param state current state
   * @param callbackContext information about callbacks
   * @param transactionHash id of the transaction initiating this transfer
   * @return updated state
   */
  @Callback(Callbacks.TRANSFER_CALLBACK)
  public ByocTokenContractState transferCallback(
      SysContractContext context,
      ByocTokenContractState state,
      CallbackContext callbackContext,
      Hash transactionHash) {

    PendingTransfer pendingTransfer = state.getPendingTransfer(transactionHash);
    BlockchainAddress spender = pendingTransfer.spender();
    BlockchainAddress sender = pendingTransfer.sender();
    BlockchainAddress receiver = pendingTransfer.receiver();
    NonZeroUnsigned128 amount = pendingTransfer.amount();

    ByocTokenContractState updatedState = state.removePendingTransfer(transactionHash);
    SystemEventCreator invocationCreator = context.getInvocationCreator();
    if (commitRequestSucceeded(callbackContext.results())) {
      commitTransfer(transactionHash, sender, receiver, invocationCreator);
    } else {
      if (!sender.equals(spender)) {
        // Re-add allowance if a TRANSFER_FROM failed
        updatedState = reAddAllowance(updatedState, sender, spender, amount);
      }
      abortTransfer(transactionHash, sender, receiver, invocationCreator);
    }
    return updatedState;
  }

  private ByocTokenContractState reAddAllowance(
      ByocTokenContractState state,
      BlockchainAddress sender,
      BlockchainAddress spender,
      NonZeroUnsigned128 transferAmount) {
    Unsigned256 existingAmount = state.getAllowances(sender).getAllowance(spender);
    Unsigned256 totalAmount = existingAmount.add(transferAmount.getValue());
    NonZeroUnsigned128 allowance = getOverflowSafeAllowance(totalAmount);

    return state.approve(sender, spender, allowance);
  }

  /**
   * Checks the allowance for potential overflow. If the supplied allowance is too large, {@link
   * NonZeroUnsigned128#MAX_VALID_VALUE} is returned.
   *
   * @param totalAllowance The total amount of allowance to check against
   * @return {@code totalAllowance} or {@link NonZeroUnsigned128#MAX_VALID_VALUE} whichever is
   *     smaller
   */
  private NonZeroUnsigned128 getOverflowSafeAllowance(Unsigned256 totalAllowance) {
    Unsigned256 overflowingAmount = NonZeroUnsigned128.MAX_VALID_VALUE.add(Unsigned256.ONE);
    if (totalAllowance.compareTo(overflowingAmount) >= 0) {
      return new NonZeroUnsigned128(NonZeroUnsigned128.MAX_VALID_VALUE);
    } else {
      return new NonZeroUnsigned128(totalAllowance);
    }
  }

  /**
   * Checks if the commit-request phase of the two-phase-commit transfer succeeded.
   *
   * @param executionResults execution results of the commit-request phase events
   * @return true if the commit-request phase succeeded otherwise false
   */
  private static boolean commitRequestSucceeded(
      FixedList<CallbackContext.ExecutionResult> executionResults) {
    // Expects two results with empty return values
    return executionResults.get(0).isSucceeded() && executionResults.get(1).isSucceeded();
  }

  private static void initiateTransfer(
      BlockchainAddress sender,
      BlockchainAddress receiver,
      NonZeroUnsigned128 amount,
      Hash transactionHash,
      String byocSymbol,
      SystemEventManager eventManager) {
    if (sender.equals(receiver)) {
      throw new IllegalArgumentException("Cannot transfer between same account");
    }
    eventManager.updateLocalAccountPluginState(
        LocalPluginStateUpdate.create(
            sender,
            AccountPluginRpc.initiateWithdraw(
                sender.getType() != BlockchainAddress.Type.ACCOUNT,
                transactionHash,
                amount,
                byocSymbol)));

    eventManager.updateLocalAccountPluginState(
        LocalPluginStateUpdate.create(
            receiver, AccountPluginRpc.initiateDeposit(transactionHash, amount, byocSymbol)));

    eventManager.registerCallback(
        Callbacks.transferCallback(transactionHash), TWO_PHASE_CALLBACK_GAS_COST);
  }

  private static void commitTransfer(
      Hash transactionHash,
      BlockchainAddress sender,
      BlockchainAddress receiver,
      SystemEventCreator eventCreator) {
    byte[] commitRpc = AccountPluginRpc.commitTransfer(transactionHash);
    eventCreator.updateLocalAccountPluginState(LocalPluginStateUpdate.create(sender, commitRpc));
    eventCreator.updateLocalAccountPluginState(LocalPluginStateUpdate.create(receiver, commitRpc));
  }

  private static void abortTransfer(
      Hash transactionHash,
      BlockchainAddress sender,
      BlockchainAddress receiver,
      SystemEventCreator eventCreator) {
    byte[] abortRpc = AccountPluginRpc.abortTransfer(transactionHash);
    eventCreator.updateLocalAccountPluginState(LocalPluginStateUpdate.create(sender, abortRpc));
    eventCreator.updateLocalAccountPluginState(LocalPluginStateUpdate.create(receiver, abortRpc));
  }

  private static void ensure(boolean predicate, String errorString) {
    if (!predicate) {
      throw new RuntimeException(errorString);
    }
  }
}
