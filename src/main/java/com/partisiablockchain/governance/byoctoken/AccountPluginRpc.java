package com.partisiablockchain.governance.byoctoken;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.Hash;
import com.secata.stream.SafeDataOutputStream;

final class AccountPluginRpc {

  static final int INITIATE_BYOC_TRANSFER_SENDER_OLD = 31;
  static final int INITIATE_BYOC_TRANSFER_SENDER = 63;
  static final int INITIATE_BYOC_TRANSFER_RECIPIENT = 32;
  static final int COMMIT_TRANSFER = 13;
  static final int ABORT_TRANSFER = 14;

  @SuppressWarnings("unused")
  private AccountPluginRpc() {}

  static byte[] initiateWithdraw(
      boolean senderIsContract, Hash transactionHash, NonZeroUnsigned128 amount, String symbol) {
    return SafeDataOutputStream.serialize(
        stream -> {
          if (senderIsContract) {
            stream.writeByte(INITIATE_BYOC_TRANSFER_SENDER_OLD);
          } else {
            stream.writeByte(INITIATE_BYOC_TRANSFER_SENDER);
            stream.writeOptional(BlockchainAddress::write, null);
          }
          transactionHash.write(stream);
          amount.getValue().write(stream);
          stream.writeString(symbol);
        });
  }

  static byte[] initiateDeposit(Hash transactionHash, NonZeroUnsigned128 amount, String symbol) {
    return SafeDataOutputStream.serialize(
        stream -> {
          stream.writeByte(INITIATE_BYOC_TRANSFER_RECIPIENT);
          transactionHash.write(stream);
          amount.getValue().write(stream);
          stream.writeString(symbol);
        });
  }

  public static byte[] commitTransfer(Hash transactionHash) {
    return SafeDataOutputStream.serialize(
        stream -> {
          stream.writeByte(COMMIT_TRANSFER);
          transactionHash.write(stream);
        });
  }

  public static byte[] abortTransfer(Hash transactionHash) {
    return SafeDataOutputStream.serialize(
        stream -> {
          stream.writeByte(ABORT_TRANSFER);
          transactionHash.write(stream);
        });
  }
}
