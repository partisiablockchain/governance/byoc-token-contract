package com.partisiablockchain.governance.byoctoken;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.tree.AvlTree;

/** Contract state for {@link ByocTokenContract}. */
@Immutable
public final class ByocTokenContractState implements StateSerializable {
  private final String byocSymbol;
  private final AvlTree<BlockchainAddress, Allowances> allowed;
  private final AvlTree<Hash, PendingTransfer> pendingTransfers;

  @SuppressWarnings("unused")
  private ByocTokenContractState() {
    this.byocSymbol = null;
    this.allowed = null;
    this.pendingTransfers = null;
  }

  ByocTokenContractState(
      String byocSymbol,
      AvlTree<BlockchainAddress, Allowances> allowed,
      AvlTree<Hash, PendingTransfer> pendingTransfers) {
    this.byocSymbol = byocSymbol;
    this.allowed = allowed;
    this.pendingTransfers = pendingTransfers;
  }

  static ByocTokenContractState initial(String symbol) {
    return new ByocTokenContractState(symbol, AvlTree.create(), AvlTree.create());
  }

  static ByocTokenContractState migrateState(StateAccessor oldState) {
    String byocSymbol = oldState.get("byocSymbol").stringValue();
    AvlTree<BlockchainAddress, Allowances> allowances =
        StateAccessorUtil.toAvlTree(
            oldState.get("allowed"), Allowances::createFromStateAccessor, BlockchainAddress.class);
    AvlTree<Hash, PendingTransfer> pendingTransfers =
        StateAccessorUtil.toAvlTree(
            oldState.get("pendingTransfers"), PendingTransfer::createFromStateAccessor, Hash.class);
    return new ByocTokenContractState(byocSymbol, allowances, pendingTransfers);
  }

  String getByocSymbol() {
    return byocSymbol;
  }

  Allowances getAllowances(BlockchainAddress from) {
    return allowed.containsKey(from) ? allowed.getValue(from) : Allowances.initial();
  }

  ByocTokenContractState approve(
      BlockchainAddress sender, BlockchainAddress spender, NonZeroUnsigned128 amount) {
    Allowances updatedAllowances = getAllowances(sender).approve(spender, amount);
    return new ByocTokenContractState(
        byocSymbol, allowed.set(sender, updatedAllowances), pendingTransfers);
  }

  ByocTokenContractState reduce(
      BlockchainAddress spender, BlockchainAddress sender, NonZeroUnsigned128 amount) {
    Allowances reducedAllowance = getAllowances(sender).reduce(spender, amount);

    AvlTree<BlockchainAddress, Allowances> updatedAllowances;
    if (reducedAllowance.isEmpty()) {
      updatedAllowances = allowed.remove(sender);
    } else {
      updatedAllowances = allowed.set(sender, reducedAllowance);
    }
    return new ByocTokenContractState(byocSymbol, updatedAllowances, pendingTransfers);
  }

  PendingTransfer getPendingTransfer(Hash transactionHash) {
    return pendingTransfers.getValue(transactionHash);
  }

  ByocTokenContractState removePendingTransfer(Hash transactionHash) {
    return new ByocTokenContractState(
        byocSymbol, allowed, pendingTransfers.remove(transactionHash));
  }

  ByocTokenContractState addPendingTransfer(
      Hash transactionHash,
      BlockchainAddress spender,
      BlockchainAddress sender,
      BlockchainAddress receiver,
      NonZeroUnsigned128 amount) {
    return new ByocTokenContractState(
        byocSymbol,
        allowed,
        pendingTransfers.set(
            transactionHash, new PendingTransfer(spender, sender, receiver, amount)));
  }
}
