package com.partisiablockchain.governance.byoctoken;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;

@Immutable
record PendingTransfer(
    BlockchainAddress spender,
    BlockchainAddress sender,
    BlockchainAddress receiver,
    NonZeroUnsigned128 amount)
    implements StateSerializable {

  public static PendingTransfer createFromStateAccessor(StateAccessor oldState) {
    BlockchainAddress spender = oldState.get("spender").blockchainAddressValue();
    BlockchainAddress sender = oldState.get("sender").blockchainAddressValue();
    BlockchainAddress receiver = oldState.get("receiver").blockchainAddressValue();
    NonZeroUnsigned128 amount = NonZeroUnsigned128.createFromStateAccessor(oldState.get("amount"));
    return new PendingTransfer(spender, sender, receiver, amount);
  }
}
