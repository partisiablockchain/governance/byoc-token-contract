package com.partisiablockchain.governance.byoctoken;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.tree.AvlTree;

@Immutable
final class Allowances implements StateSerializable {
  private final AvlTree<BlockchainAddress, NonZeroUnsigned128> allowances;

  @SuppressWarnings("unused")
  private Allowances() {
    allowances = null;
  }

  private Allowances(AvlTree<BlockchainAddress, NonZeroUnsigned128> allowances) {
    this.allowances = allowances;
  }

  static Allowances initial() {
    return new Allowances(AvlTree.create());
  }

  Allowances approve(BlockchainAddress spender, NonZeroUnsigned128 amount) {
    return new Allowances(allowances.set(spender, amount));
  }

  Allowances reduce(BlockchainAddress spender, NonZeroUnsigned128 amount) {
    Unsigned256 currentAmount = getAllowance(spender);
    Unsigned256 updatedAmount = currentAmount.subtract(amount.getValue());
    AvlTree<BlockchainAddress, NonZeroUnsigned128> updatedAllowances;
    if (updatedAmount.equals(Unsigned256.ZERO)) {
      updatedAllowances = allowances.remove(spender);
    } else {
      updatedAllowances = allowances.set(spender, new NonZeroUnsigned128(updatedAmount));
    }
    return new Allowances(updatedAllowances);
  }

  Unsigned256 getAllowance(BlockchainAddress spender) {
    return allowances.containsKey(spender)
        ? allowances.getValue(spender).getValue()
        : Unsigned256.ZERO;
  }

  public static Allowances createFromStateAccessor(StateAccessor oldState) {
    AvlTree<BlockchainAddress, NonZeroUnsigned128> allowances =
        StateAccessorUtil.toAvlTree(
            oldState.get("allowances"),
            NonZeroUnsigned128::createFromStateAccessor,
            BlockchainAddress.class);
    return new Allowances(allowances);
  }

  boolean isEmpty() {
    return allowances.size() == 0;
  }
}
